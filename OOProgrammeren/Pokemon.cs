﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOProgrammeren
{
    class Pokemon
    {
        private int maxHP = 20;

        public int MaxHP
        {
            get { return maxHP; }
            set
            {

                if (maxHP < 20)
                {
                    value = 20;
                }
                else if (maxHP > 1000)
                {
                    value = 1000;
                }
                else
                {
                    maxHP = value;
                }
            }
        }

        private int hP = 20;

        public int HP
        {
            get { return hP; }
            set
            {
                if (hP > MaxHP)
                {
                    hP = MaxHP;
                }
                else if (hP <= 0)
                {
                    hP = 0;
                }
                else
                {
                    hP = value;
                }
            }
        }

        public PokeTypes PokeType { get; set; }

        public PokeSpecies PokeSpecies { get; set; }

        public Pokemon()
        {
                
        }

        public Pokemon(int maxHP, int hP, PokeTypes pokeType, PokeSpecies pokeSpecies)
        {
            MaxHP = maxHP;
            HP = hP;
            PokeType = pokeType;
            PokeSpecies = pokeSpecies;
        }

        public Pokemon(int maxHp, PokeTypes pokeType, PokeSpecies pokeSpecies): this(maxHp, 40 ,pokeType, pokeSpecies)
        {
            MaxHP = maxHP;
            HP = HP / 2;
            PokeType = pokeType;
            PokeSpecies = pokeSpecies;
        }


        public void Attack()
        {



            if (PokeType == PokeTypes.Elelectric)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(PokeSpecies.ToString().ToUpper());
            }
            else if (PokeType == PokeTypes.Fire)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(PokeSpecies.ToString().ToUpper());
            }
            else if (PokeType == PokeTypes.Grass)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(PokeSpecies.ToString().ToUpper());
            }
            else if (PokeType == PokeTypes.Water)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(PokeSpecies.ToString().ToUpper());
            }
        }

        public static void MakePokemon()
        {

            Pokemon pokemon1 = new Pokemon(100,50, PokeTypes.Grass, PokeSpecies.Bulbasaur);
            pokemon1.Attack();

            Pokemon pokemon2 = new Pokemon(100, 50,PokeTypes.Fire,PokeSpecies.Charmander);
            pokemon2.Attack();


        }


        public static Pokemon FirstConsciousPokemon(Pokemon[] pokemons)
        {

            for (int i = 0; i < pokemons.Length; i++)
            {
                if (pokemons[i].HP >= 1)
                {
                    return pokemons[i];
                }
            }

            return null;
        }

        public static void TestConsciousPokemon()
        {

            Pokemon pokemon1 = new Pokemon();
            pokemon1.PokeSpecies = PokeSpecies.Bulbasaur;
            pokemon1.PokeType = PokeTypes.Grass;
            pokemon1.HP = 0;

            Pokemon pokemon2 = new Pokemon();
            pokemon2.PokeSpecies = PokeSpecies.Charmander;
            pokemon2.PokeType = PokeTypes.Fire;
            pokemon2.HP = 0;


            Pokemon pokemon3 = new Pokemon();
            pokemon3.PokeSpecies = PokeSpecies.Squirtle;
            pokemon3.PokeType = PokeTypes.Water;
            pokemon3.HP = 2;

            Pokemon[] pokemons_ar = new Pokemon[] { pokemon1, pokemon2, pokemon3 };
            Pokemon first = FirstConsciousPokemon(pokemons_ar);
            if (first == null)
            {
                Console.WriteLine("Al je Pokémon zijn KO! Haast je naar het Pokémon center.");
            }
            else
            {
                first.Attack();
            }

        }

        public static void RestoreHP(Pokemon poke, int newHP)
        {
           poke.HP = newHP;
        }


        public static void DemoRestoreHP()
        {

            Pokemon pokemon1 = new Pokemon();
            pokemon1.PokeSpecies = PokeSpecies.Bulbasaur;
            pokemon1.PokeType = PokeTypes.Grass;
            pokemon1.HP = 0;

            Pokemon pokemon2 = new Pokemon();
            pokemon2.PokeSpecies = PokeSpecies.Charmander;
            pokemon2.PokeType = PokeTypes.Fire;
            pokemon2.HP = 0;


            Pokemon pokemon3 = new Pokemon();
            pokemon3.PokeSpecies = PokeSpecies.Squirtle;
            pokemon3.PokeType = PokeTypes.Water;
            pokemon3.HP = 2;

            Pokemon[] pokemon = new Pokemon[] { pokemon1, pokemon2, pokemon3 };
            Pokemon first = FirstConsciousPokemon(pokemon);

            // aanmaken van array bewusteloze Pokemon van 4 soorten zoals eerder: zelf doen
            for (int i = 0; i < pokemon.Length; i++)
            {
                Pokemon.RestoreHP(pokemon[i], pokemon[i].MaxHP);
            }
            for (int i = 0; i < pokemon.Length; i++)
            {
                Console.WriteLine(pokemon[i].HP);
            }
        }

        public static void ConstructPokemonChained()
        {
            Pokemon vincent = new Pokemon(40, PokeTypes.Water, PokeSpecies.Squirtle);
            Console.WriteLine($"De nieuwe {vincent.PokeSpecies} heeft maximum {vincent.MaxHP} HP en heeft momenteel {vincent.HP} HP.");
        }

        public static void DemonstrateCounter() 
        {
            int GrassTeller = 0, WaterTeller = 0, ElelectricTeller= 0, FireTeller = 0;
            Random rnd = new Random();
           

            Pokemon poke1 = new Pokemon(100, PokeTypes.Grass, PokeSpecies.Bulbasaur);
            Pokemon poke2 = new Pokemon(100, PokeTypes.Water, PokeSpecies.Squirtle);
            Pokemon poke3 = new Pokemon(100, PokeTypes.Elelectric, PokeSpecies.Pikachu);
            Pokemon poke4 = new Pokemon(100, PokeTypes.Fire, PokeSpecies.Charmander);
            Pokemon poke5 = new Pokemon(90, PokeTypes.Grass, PokeSpecies.Bulbasaur);

            Pokemon[] pokemonArray = { poke1, poke2, poke3, poke4, poke5 };

            for (int i = 0; i < pokemonArray.Length ; i++)
            {
                for (int x = 0; x < rnd.Next(5, 11); x++)
                {
                    pokemonArray[i].Attack();
                    switch (pokemonArray[i].PokeType)
                    {
                        case PokeTypes.Grass:
                            GrassTeller++;
                            break;
                        case PokeTypes.Fire:
                            FireTeller++;
                            break;
                        case PokeTypes.Water:
                            WaterTeller++;
                            break;
                        case PokeTypes.Elelectric:
                            ElelectricTeller++;
                            break;
                        default:
                            break;
                    }
                }
            }

            Console.WriteLine(GrassTeller);
            Console.WriteLine(FireTeller);
            Console.WriteLine(WaterTeller);
            Console.WriteLine(ElelectricTeller);
        }
    }
}

