﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOProgrammeren
{
    class Ticket
    {
        Random rnd = new Random();

        public byte Prize { get; set; }

        public Ticket()
        {
            Prize = (byte)rnd.Next(1,101);
        }

        public static void Raffle() 
        {
            for (int i = 0; i < 10; i++)
            {
                Ticket tick = new Ticket();
                Console.WriteLine($"Waarde van het lotje: {tick.Prize}"); 
            }
        }
    }
}
