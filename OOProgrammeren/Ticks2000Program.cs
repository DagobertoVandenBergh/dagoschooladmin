﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOProgrammeren
{
    class Ticks2000Program
    {
        public static void Main()
        {
            DateTime firstOf2000 = new DateTime(2000, 01, 01);

            DateTime nowDateOfToday = DateTime.Now;

            long elapsedTicks = nowDateOfToday.Ticks - firstOf2000.Ticks;
            Console.WriteLine("{0:N0} ticks", elapsedTicks);

        }
    }
}
