﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum Honors
    {
        Niet_geslaagd, Voldoende, Onderscheiding, Grote_onderscheiding
    }
    class ResultV2
    {

        public byte Percentage {get;set;}

        public Enum ComputeHonors()
        {
            
            if (Percentage < 50)
            {
                return Honors.Niet_geslaagd;
            }
            else if (Percentage < 68)
            {
                return Honors.Voldoende;
            }
            else if (Percentage < 75)
            {
                return Honors.Onderscheiding;
            }
            else if (Percentage < 85)
            {
                return Honors.Grote_onderscheiding;
            }
            else
            {
                return null;
            }
        }
        public static void Main()
        {
            ResultV2 result1 = new ResultV2();
            result1.Percentage = 40;
            result1.ComputeHonors();

            ResultV2 result2 = new ResultV2();
            result2.Percentage = 60;
            result2.ComputeHonors();

            ResultV2 result3 = new ResultV2();
            result3.Percentage = 80;
            result3.ComputeHonors();

            ResultV2 result4 = new ResultV2();
            result4.Percentage = 90;
            result4.ComputeHonors();
        }

    }



}

