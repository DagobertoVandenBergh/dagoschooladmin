﻿using OOProgrammeren;
using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {

            int choice;
            while (true)
            {
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. Vormen tekenen (h8-vormen)");
                Console.WriteLine("2. Auto's laten rijden ");
                Console.WriteLine("3.Patiënten tonen (h8-patienten)");
                Console.WriteLine("4.Honden laten blaffen (h8-blaffende-honden)");
                Console.WriteLine("5. H8-dag-van-de-week");
                Console.WriteLine("6.H8-ticks-sinds-2000");
                Console.WriteLine("7");
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        ShapesBuilder shapes = new ShapesBuilder();
                        shapes.Color = ConsoleColor.Green;
                        string line = shapes.Line(10);
                        Console.WriteLine(line);

                        shapes.Color = ConsoleColor.Red;
                        string rec = shapes.Rectangle(10,5);
                        Console.WriteLine(rec);

                        shapes.Color = ConsoleColor.Blue;
                        string tri = shapes.Triangle(10);
                        Console.WriteLine(tri);
                        break;

                    case 2:
                        Car car1 = new Car();
                        for (int i = 1; i <= 5; i++) 
                        {
                            car1.Gas();
                        }
                        string speed = car1.Speed.ToString();
                        for (int i = 1; i <= 3 ; i++)
                        {
                            car1.Break();
                        }
                        string breakSpeed = car1.Speed.ToString();

                        Console.WriteLine($"auto versnellen {speed}KM daarna Vertraag {breakSpeed}KM");

                        Console.WriteLine("Auto's laten rijden");

                        break;

                    case 3:
                        PatientProgram patient = new PatientProgram();
                        PatientProgram.Main();                        
                        break;
                    case 4:
                        Console.WriteLine("niks");
                        break;

                    case 5:
                        DayOfWeekProgram.Main();
                        break;

                    case 6:
                        Ticks2000Program.Main();
                        break;
                    case 7:
                        ArrayTimerProgram.ArrayCount();
                        break;
                    case 9:
                        ResultV1.Main();
                        break;

                    case 10:
                        ResultV2.Main();
                        break;
                    case 11:
                        FigureProgram.Main();
                        break;
                    case 12:
                        Student.Main();
                        break;
                    case 13:
                        Pokemon.DemoRestoreHP();
                        break;                   
                    case 14:
                        Pokemon.ConstructPokemonChained();
                        break;
                    case 15:
                        Pokemon.DemonstrateCounter();
                        break;
                    case 16:
                        Ticket.Raffle();
                        break;
                    default:
                        Console.WriteLine("ongeldig getal!");
                        break;
                }
            }
        }
    }
}
