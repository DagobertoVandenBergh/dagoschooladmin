﻿using System;
using System.Collections.Generic;
using System.Text;


namespace OOProgrammeren
{
    
    class Student
    {
        public string Name { get; set; }
        private byte age;

        public byte Age
        {
            get { return age; }
            set
            {
                if (age > 120)
                {
                    Console.WriteLine("Error, leeftijd klopt ni");
                }
                else
                {
                    age = value;
                }
            }
        }

        
        public Klasgroep Class { get; set; }

        private int markCommunication;

        public int MarkCommunication
        {
            get { return markCommunication; }
            set
            {
                if (markCommunication > 20 )
                {
                    Console.WriteLine("Error, cijfer mag niet boven 20");
                }
                else
                {
                    markCommunication = value;
                }
            }
        }


        private int markProgrammingPrinciples;

        public int MarkProgrammingPrinciples
        {
            get { return markProgrammingPrinciples; }
            set
            {
                if (markProgrammingPrinciples > 20)
                {
                    Console.WriteLine("Error, cijfer mag niet boven 20");
                }
                else
                {
                    markProgrammingPrinciples = value;
                }
            }
        }

        private int markWebTech;

        public int MarkWebTech
        {
            get { return markWebTech; }
            set
            {
                if (markWebTech > 20)
                {
                    Console.WriteLine("Error, cijfer mag niet boven 20");
                }
                else
                {
                    markWebTech = value;
                }
            }
        }

        public double OverallMark { get { return (Convert.ToDouble((MarkCommunication) + MarkProgrammingPrinciples + MarkWebTech) / 3); } }

        public void ShowOverview() 
        {
            Console.WriteLine($"MarkCommunication:{MarkCommunication }\nMarkProgrammingPrinciples:{MarkProgrammingPrinciples}\n" +
                $"MarkWebTech:{MarkWebTech}\nOverallMark:{OverallMark:F1}");
        }

        public  static void Main() 
        {

            Student student1 = new Student();
            student1.Class = Klasgroep.EA2;
            student1.Age = 21;
            student1.Name = "Joske Vermeulen";
            student1.MarkCommunication = 12;
            student1.MarkProgrammingPrinciples = 15;
            student1.MarkWebTech = 13;
            Console.WriteLine($"{student1.Name}, {student1.Age}\n" +
                $"Klas:{student1.Class}\n\n" +
                $"Cijferraport:\n" +
                $"*******************");
            student1.ShowOverview();
        }
    }
}
