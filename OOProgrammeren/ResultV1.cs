﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP

{
    class ResultV1
    {
        public byte Percentage
        {
            get;
            set;
        }

        public void PrintHonors()
        {
            if (Percentage < 50)
            {
                Console.WriteLine("Niet geslaagd");
            }
            else if (Percentage < 68)
            {
                Console.WriteLine("Voldoende");
            }
            else if (Percentage < 75)
            {
                Console.WriteLine("onderscheiding");
            }
            else if (Percentage < 85)
            {
                Console.WriteLine("Grote onderscheiding");
            }
            else
            {
                Console.WriteLine("Grootste onderscheiding");
            }

        }
        public static void Main()
        {
            ResultV1 result1 = new ResultV1();
            result1.Percentage = 40;
            result1.PrintHonors();

            ResultV1 result2 = new ResultV1();
            result2.Percentage = 60;
            result2.PrintHonors();

            ResultV1 result3 = new ResultV1();
            result3.Percentage = 80;
            result3.PrintHonors();

            ResultV1 result4 = new ResultV1();
            result4.Percentage = 90;
            result4.PrintHonors();
        }
    }
}