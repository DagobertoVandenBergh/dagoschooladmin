﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class PatientProgram
    {
        public static void Main()
        {
            Console.WriteLine("Hoe veel patiënten zijn er?"); // aanmaken van de nodige variabelen
            int numberOfPatients = int.Parse(Console.ReadLine());
            string[] patientNames = new string[numberOfPatients];
            string[] patientGenders = new string[numberOfPatients];
            string[] patientLifestyles = new string[numberOfPatients];
            int[] patientDays = new int[numberOfPatients];
            int[] patientMonths = new int[numberOfPatients];
            int[] patientYears = new int[numberOfPatients];

            // invullen van de rijen
            for (int i = 0; i < numberOfPatients; i++)
            {
                patientNames[i] = Console.ReadLine();
                patientGenders[i] = Console.ReadLine();
                patientLifestyles[i] = Console.ReadLine();
                patientDays[i] = int.Parse(Console.ReadLine());
                patientMonths[i] = int.Parse(Console.ReadLine());
                patientYears[i] = int.Parse(Console.ReadLine());
            }

            // afprinten van het verslag
            // variabele in twee keer toegekend om code niet te breed te maken
            for (int i = 0; i < numberOfPatients; i++)
            {
                string info = $"{patientNames[i]} ({patientGenders[i]}, {patientLifestyles[i]})";
                info += $", geboren {patientDays[i]}-{patientMonths[i]}{patientYears[i]}";
                Console.WriteLine(info);
            }
        }
    }
}
