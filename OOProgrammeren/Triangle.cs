﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOProgrammeren
{
    class Triangle
    {
        private double @base = 1.0;

        public double @Base
        {
            get { return @base; }
            set
            {
                if (@base <= 0)
                {
                    Console.WriteLine("Error");
                }
                else
                {
                    @base = value;
                }
            }
        }

        private double height = 1.0;

        public double Height
        {
            get { return height; }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Het is verboden een breedte van {value} in te stellen!");
                }
                else
                {
                    height = value;
                }
            }
        }

        public double Surface
        {
            get { return @Base * Height / 2; }

        }

    }
}
