﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOProgrammeren
{
    class Rectangle
    {
        private double width = 1.0;

        public double Width
        {
            get { return width; }
            set
            {
                if (width <= 0)
                {
                    Console.WriteLine("Error");
                }
                else
                {
                    width = value;
                }
            }
        }

        private double height = 1.0;

        public double Height
        {
            get { return height; }
            set {
                if (value  <= 0)
                {
                    Console.WriteLine($"Het is verboden een breedte van {value} in te stellen!");
                }
                else
                {
                    height = value;
                }
            }
        }


        public double Surface
        {
            get { return Height * Width; }
            
           
        }


    }
}
