﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class NumberCombination
    {
        private int eersteGetal;

        public int EersteGetal
        {
            get { return eersteGetal; }
            set { eersteGetal = value; }
        }

        public int TweedeGetal { get; set; }

        public double Sum()
        {
            return EersteGetal + TweedeGetal;            
        }

        public double Difference() 
        {
            return EersteGetal - TweedeGetal;
        }

        public double Product() 
        {
            return EersteGetal * TweedeGetal;
        }

        public double Quotient() 
        {

            double result = 0;
            if ( TweedeGetal == 0)
            {
                Console.WriteLine("Error");
            }
            else
            {
                result = EersteGetal / TweedeGetal;
            }
            return Convert.ToDouble(result);
        }

        public static void Main() 
        {

            NumberCombination pair1 = new NumberCombination();
            pair1.EersteGetal = 12;
            pair1.TweedeGetal = 34;
            Console.WriteLine("Paar:" + pair1.EersteGetal + ", " + pair1.TweedeGetal);
            Console.WriteLine("Sum = " + pair1.Sum());
            Console.WriteLine("Verschil = " + pair1.Difference());
            Console.WriteLine("Product = " + pair1.Product());
            Console.WriteLine("Quotient = " + pair1.Quotient());
        }


    }
}
