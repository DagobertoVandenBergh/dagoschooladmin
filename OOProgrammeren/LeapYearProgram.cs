﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOProgrammeren
{
    class LeapYearProgram
    {

        public static void Main() 
        {
            

            int count = 0;

            for (int i = 1800; i <= 2020; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    count++;
                }

            }
            Console.WriteLine($"{count}");
       

            
        
        }
    }
}
