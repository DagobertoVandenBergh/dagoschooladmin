﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOProgrammeren
{
    class ArrayTimerProgram
    {

        public static void ArrayCount() 
        {

            
            DateTime startArray = DateTime.Now;
            int[] myarray = new int[1000000];
            

            for ( int i = 0; i < myarray.Length; i++)
            {
                myarray[i] = i + 1;
            }
            DateTime eindArray = DateTime.Now;
            TimeSpan time = eindArray - startArray;

            Console.WriteLine($"s:{time.Milliseconds}");
        }

    }
}
