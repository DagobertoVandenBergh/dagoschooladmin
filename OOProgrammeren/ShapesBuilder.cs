﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOProgrammeren
{
    class ShapesBuilder
    {
        private ConsoleColor color;
        public ConsoleColor Color
        {
            get { return color; }
            set
            {
                color = value;
                Console.ForegroundColor = color;
            }
        }

        private char symbol;
        public char Symbol
        {
            get { return symbol; }
            set
            {
                symbol = value;
            }
        }

        public string Line(int length)
        {
            return new string(Symbol, length);
        }

        public string Line(int length, char alternateSymbol)
        {
            return new string(alternateSymbol, length);
        }


        public string Rectangle(int height, int width)
        {
            string output = "";
            for (int i = 0; i < height; i++)
                if (i < height - 1)
                {
                    output += new string('-', width);
                    output += "\n";
                }
            return output;
        }

        public string Triangle(int height)
        {
            return Triangle(height, Symbol);
        }

        public string Triangle(int height, char alternateSymbol)
        {
            string output = "";
            for (int i = 0; i < height; i++)
            {
                output += Line(i + 1, alternateSymbol);
                if (i < height - 1) 
                {
                    output += "\n *";
                }
            }
            return output;
        }



    }
}


