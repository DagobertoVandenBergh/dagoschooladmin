﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Car
    {
        private double speed;
        public double Speed
        {
            get { return speed; }
            set
            {
                if (value >= 0 && value <= 120)
                {
                    speed = value;
                }      
            }
        }

        private double odometer;
        public double Odometer
        {
            get { return odometer; }
            set
            {
                odometer = value;
            }
        }

        public void Gas() 
        {
            double oldSpeed = Speed;
            Speed += 10;
            Odometer -= ((oldSpeed + Speed)/2) / 60;

        }

        public void Break() 
        {
            double oldSpeed = Speed;
            Speed -= 10;
            Odometer -= ((oldSpeed + Speed) / 2) / 60;
        }

    }
}
