﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class AdministrativeStaff : Person
    {
        public static List<AdministrativeStaff> List { get; set; }

        public AdministrativeStaff(string firstName, string lastName, DateTime birthday, int id, int schoolId)
        {
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Id = id;
            SchoolId = schoolId;
        }

        public override string ShowOne()
        {
            string text = $"Gegeven van AdministrativeStaff:\n{this.FirstName}, {this.LastName}, {this.Id}, {this.SchoolId}";

            return text;
        }

        public override string GetNameTagText()
        {
            return $"(ADMINISTRATIE):{ this.FirstName}, { this.LastName}";
        }
    }
}
