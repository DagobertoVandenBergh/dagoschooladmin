﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Lecturer : Person
    {

        public static List<Lecturer> List { get; set; }

        public Lecturer(string firstName, string lastName, DateTime birthday, int id, int schoolId)
        {
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Id = id;
            SchoolId = schoolId;
        }

        public static string ShowAll()
        {
            string text = "Lijst van Lectoren:\n";

            foreach (var Student in List)
            {
                text += $"{Student.FirstName}, {Student.LastName}, {Student.Birthday.ToShortDateString()}, {Student.Id}, {Student.SchoolId}\n";
            }

            return text;
        }

        public string ShowCourses() 
        {

            return $"vakken van lecturen";
        }

        public override string ShowOne()
        {
            string text = $"Gegeven van de Lector:{this.FirstName}, {this.LastName}, {this.Birthday}, {this.Id}, {this.SchoolId}";

            return text;
        }

        public override string GetNameTagText()
        {
            return $"(LECTOR):{ this.FirstName}, { this.LastName}";
        }



    }
}
