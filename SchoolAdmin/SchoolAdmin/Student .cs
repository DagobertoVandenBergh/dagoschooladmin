﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Student : Person
    {


        public static List<Student> List { get; set; }

        public Student(string firstName, string lastName, DateTime birthday, int id, int schoolId)
        {
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Id = id;
            SchoolId = schoolId;
        }

        public static string ShowAll()
        {
            string text = "Lijst van Studenten:\n";

            foreach (var Student in List)
            {
                text += $"{Student.FirstName}, {Student.LastName}, {Student.Birthday.ToShortDateString()}, {Student.Id}, {Student.SchoolId}\n";
            }

            return text;
        }

        public override string ShowOne()
        {
            string text = $"Gegeven van de school:{this.FirstName}, {this.LastName}, {this.Birthday}, {this.Id}, {this.SchoolId}";

            return text;
        }

    }
}
