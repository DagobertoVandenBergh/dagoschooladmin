﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            List<School> schoolList = new List<School>();
            List<Student> studentList = new List<Student>();
            List<Lecturer> lecturersList = new List<Lecturer>();
            List<AdministrativeStaff> administrativeStaffsList = new List<AdministrativeStaff>();

            School school1 = new School("GO! BS de Spits", "Thonetlaan 106", "2050", "Antwerpen", 1);
            School school2 = new School("GO! Koninklijk Atheneum Deurne", "Fr.Craeybeckxlaan 22", "2100", "Deurne", 2);
            schoolList.Add(school1);
            schoolList.Add(school2);
            School.List = schoolList;
            Console.WriteLine(School.ShowAll());

            Student student1 = new Student("Mohamed"," El Farisi", new DateTime(1987,12,06), 1, 1);
            Student student2 = new Student("Sarah", "Jansens", new DateTime(1991,10,21), 2, 1);
            Student student3 = new Student("Bart", "Jansens", new DateTime(1990,10,21), 2, 3);
            Student student4 = new Student("Farah", "El Farisi",new DateTime(1987,12,06), 1, 4);
            studentList.Add(student1);
            studentList.Add(student2);
            studentList.Add(student3);
            studentList.Add(student4);
            Student.List = studentList;
            Console.WriteLine(Student.ShowAll());

            Lecturer lecturer1 = new Lecturer("Adem", "Kaya",new DateTime( 1976,12,01), 1, 1);
            Lecturer lecturer2 = new Lecturer("Anne", "Wouters",new DateTime( 1968,04 , 03), 2, 2);
            lecturersList.Add(lecturer1);
            lecturersList.Add(lecturer2);
            Lecturer.List = lecturersList;
            Console.WriteLine(Lecturer.ShowAll());

            AdministrativeStaff administrativeStaff1 = new AdministrativeStaff("Raul","Jacob", new DateTime(1985, 11 , 01), 1, 1);
            administrativeStaffsList.Add(administrativeStaff1);
            AdministrativeStaff.List = administrativeStaffsList;
            Console.WriteLine(administrativeStaff1.ShowOne());

        }
    }
}
