﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class School
    {
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrEmpty(name))
                {
                    name = value;
                } 
            }
        }

        public string Street { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public int Id { get; set; }

        public static List<School> List { get; set; }

        public School(string name, string street, string postalCode, string city, int id)
        {
            Name = name;
            Street = street;
            PostalCode = postalCode;
            City = city;
            Id = id;
        }

        public static string ShowAll() 
        {
            string text = "Lijst van scholen:\n";

            foreach (var School in List)
            {
                text += $"{School.Name}, {School.Street}, {School.City}, {School.Id}\n";
            }

            return text;
        }

        public string ShowOne() 
        {
         string text = $"Gegeven van de school:{ this.Name}, { this.Street}, { this.City}, { this.Id}";

            return text;
        }

    }
}
